using System.Linq;
using CakeClub.Entities;
using CakeClub.Models;

namespace CakeClub
{
    public class AutoMapperProfile : AutoMapper.Profile
    {
        public AutoMapperProfile ()
        {
            CreateMap<Member, MemberDto>()
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(m => $"{m.FirstName} {m.LastName}"))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(m => m.User.UserName));
            CreateMap<Vote, VoteDto>();
            CreateMap<Week, WeekDto>()
                .ForMember(dest => dest.BakerFullName, opt => opt.MapFrom(m => $"{m.Baker.FirstName} {m.Baker.LastName}"));
            //CreateMap<User, UserDto>();
            CreateMap<Cake, CakeDto>()
                .ForMember(dest => dest.Grade, opt => 
                    opt.MapFrom(cake => cake.Votes.Average(vote => vote.Grade)));
        }
    }
}