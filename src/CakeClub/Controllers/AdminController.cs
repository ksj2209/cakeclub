using CakeClub.Entities;
using CakeClub.Infrastructure.Services;
using CakeClub.Models.AdminViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CakeClub.Controllers
{
    [Authorize(Roles="Administrator")]
    [Route("[controller]")]
    public class AdminController : Controller
    {
        private readonly SignInManager<User> _signInManager;
        private readonly EmailService _emailService;
        private readonly ILogger _logger;
        private readonly UserService _userService;

        public AdminController(
            SignInManager<User> signInManager,
            EmailService emailService,
            ILoggerFactory loggerFactory,
            UserService memberService,
            UserService userService)
        {
            _signInManager = signInManager;
            _emailService = emailService;
            _logger = loggerFactory.CreateLogger<AdminController>();
            _userService = userService;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var members = await _userService.GetMembers();

            var model = new AdminIndexViewModel
            {
                Members = members
            };

            return View(model);
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> EditMember(int id)
        {
            var member = await _userService.GetMember(id);

            var vm = new EditMemberViewModel
            {
                Member = member,
                IsAdmin = await _userService.IsAdministrator(member.Id)
            };

            return View(vm);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> EditMember(int id, EditMemberViewModel vm)
        {
            await _userService.UpdateMember(id, vm.Member);

            if (vm.IsAdmin)
            {
                await _userService.AddToAdministrators(vm.Member.Id);
            }
            else
            {
                await _userService.RemoveFromAdministrators(vm.Member.Id);
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> ResetMemberPassword(int id)
        {
            var member = await _userService.GetMember(id);

            if (string.IsNullOrEmpty(member.UserEmail)) return View("Error");

            await _userService.ResetPassword(member.UserEmail);

            return RedirectToAction(nameof(Index));
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> ActivateMember(int id)
        {
            await _userService.ActivateMember(id);
            
            return RedirectToAction(nameof(Index));
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> DeactivateMember(int id)
        {
            await _userService.DeactivateMember(id);

            return RedirectToAction(nameof(Index));
        }

        [HttpGet("[action]")]
        public IActionResult Invite()
        {
            return View();
        }

        [HttpPost("[action]")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Invite(InviteViewModel model)
        {
            if (ModelState.IsValid)
            {
                await _userService.Invite(model.Email);

                return RedirectToAction(nameof(Index));
            }

            return View(model);
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        #endregion
    }
}