using AutoMapper.QueryableExtensions;
using CakeClub.Infrastructure;
using CakeClub.Infrastructure.Services;
using CakeClub.Models;
using CakeClub.Models.CakesViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CakeClub.Controllers
{
    [Authorize]
    [Route("[controller]")]
    public class CakesController : Controller
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly VoteService _voteService;
        private readonly UserService _userService;
        private readonly UserProvider _userProvider;
        private readonly ILogger<CakesController> _logger;
        private readonly CakeService _cakeService;
        private readonly EmailService _emailService;

        public CakesController (ApplicationDbContext dbContext, UserService userService, UserProvider userProvider, VoteService voteService, CakeService cakeService, EmailService emailService, ILogger<CakesController> logger)
        {
            _dbContext = dbContext;
            _voteService = voteService;
            _userService = userService;
            _userProvider = userProvider;
            _cakeService = cakeService;
            _emailService = emailService;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var cakes = await _cakeService.GetCakes();

            var vm = new CakesViewModel();
            vm.Cakes = cakes;

            return View(vm);
        }

        [HttpGet("{cakeId}")]
        public async Task<IActionResult> Cake(int cakeId)
        {
            var cake = await _cakeService.GetCake(cakeId);
            var votes = await _cakeService.GetVotes(cakeId);

            var vm = new CakeViewModel();
            vm.Cake = cake;
            vm.Votes = votes;
            vm.HasVoted = await _voteService.HasCurrentMemberVoted(cakeId);

            return View(vm);
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> CreateCake(DateTime? time = null)
        {
            var vm = new CreateCakeViewModel();

            if (time.HasValue)
                vm.Date = time.Value;
            else
                vm.Date = DateTime.Now;

            vm.Time = vm.Date.AddHours(13).AddMinutes(55);
            vm.Members = await _userService.GetActiveMembers();

            return View(vm);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> CreateCake(CreateCakeViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return View(vm);
            }

            var when = new DateTime(vm.Date.Year, vm.Date.Month, vm.Date.Day, vm.Time.Hour, vm.Time.Minute, 0);

            int id = await _cakeService.CreateCake(vm.Name, when, vm.Recipe, vm.BakeSelf, vm.VotesSent, vm.MemberId);

            var cake = await _cakeService.GetCake(id);

            await _emailService.SendCakeInvites(cake);

            return RedirectToAction(nameof(Cake), new { cakeId = id });
        }

        [HttpGet("{cakeId}/edit")]
        public async Task<IActionResult> EditCake(int cakeId)
        {
            var cake = await _cakeService.GetCake(cakeId);
            
            var vm = new EditCakeViewModel
            {
                CakeId = cake.Id,
                Name = cake.Name,
                Recipe = cake.Recipe,
                BakeSelf = cake.BakeSelf,
                Date = cake.EventTime,
                Time = cake.EventTime,
                MemberId = cake.CreatorId,
                Members = await _userService.GetActiveMembers()
            };

            return View(vm);
        }

        [HttpPost("{cakeId}/edit")]
        public async Task<IActionResult> EditCake(int cakeId, EditCakeViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return View(vm);
            }

            var when = new DateTime(vm.Date.Year, vm.Date.Month, vm.Date.Day, vm.Time.Hour, vm.Time.Minute, 0);
            await _cakeService.UpdateCake(cakeId, vm.Name, when, vm.Recipe, vm.BakeSelf, vm.MemberId);

            return RedirectToAction(nameof(Cake), new { cakeId = cakeId });
        }

        [HttpGet("{cakeId}/[action]")]
        [Authorize()]
        public async Task<IActionResult> AddVote(int cakeId)
        {
            var vm = new AddVoteViewModel
            {
                CakeId = cakeId
            };

            vm.Members = await _userService.GetActiveMembers();

            return View(vm);
        }

        [HttpPost("{cakeId}/[action]")]
        public async Task<IActionResult> AddVote(int cakeId, AddVoteViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return View(vm);
            }

            await _voteService.AddVote(vm.MemberId, cakeId, vm.Grade, vm.Participated);

            return RedirectToAction(nameof(Cake), new { cakeId = cakeId });
        }

        [HttpPost("{cakeId}/delete")]
        public async Task<IActionResult> DeleteCake(int cakeId)
        {
            var cake = await _cakeService.GetCake(cakeId);

            await _cakeService.DeleteCake(cakeId);

            return RedirectToAction(nameof(Index));
        }

        [HttpPost("{cakeId}/uploadimage")]
        public async Task<IActionResult> UploadImage(int cakeId, ICollection<IFormFile> files)
        {
            return RedirectToAction(nameof(Cake), new { cakeId = cakeId });
        }

        [HttpPost("{cakeId}/startvote")]
        public async Task<IActionResult> StartVoting(int cakeId)
        {
            // Find all active members and send vote emails
            var members = await _userService.GetActiveMembers();

            foreach (var member in members)
            {
                await _voteService.SendVoteEmail(member.Id, cakeId);
            }

            return RedirectToAction("Cake", new { cakeId = cakeId });
        }

        [HttpGet("{cakeId}/vote")]
        public async Task<IActionResult> Vote(int cakeId)
        {
            var vm = new VoteViewModel();

            var currentMember = await _userProvider.GetCurrentMember();

            if (currentMember == null)
            {
                ModelState.AddModelError("", "An admin user cannot vote!");

                return View(vm);
            }

            if (await _dbContext.Votes.AnyAsync(x => x.Member.Id == currentMember.Id && x.Cake.Id == cakeId))
            {
                ModelState.AddModelError("", "You have already voted for this cake");

                return View(vm);
            }

            var cake = await _dbContext.Cakes.Where(x => x.Id == cakeId).ProjectTo<CakeDto>().FirstOrDefaultAsync();
            var member = await _dbContext.Members.Where(x => x.Id == currentMember.Id).ProjectTo<MemberDto>().FirstOrDefaultAsync();

            if (member == null)
            {
                ModelState.AddModelError("", "User not found");
            }

            if (cake == null)
            {
                ModelState.AddModelError("", "Cake not found");
            }

            vm.Member = member;
            vm.Cake = cake;
            vm.CakeId = cake.Id;
            vm.MemberId = currentMember.Id;

            return View(vm);
        }

        [HttpGet("/directvote")]
        [AllowAnonymous]
        public async Task<IActionResult> VoteDirectly(int cakeId, int memberId, string token, bool participated, int? grade = null)
        {
            if (!_voteService.Validate(memberId, cakeId, token))
            {
                return BadRequest("The provided token is invalid");
            }

            if (await _dbContext.Votes.AnyAsync(x => x.Member.Id == memberId && x.Cake.Id == cakeId))
            {
                return BadRequest("You have already voted for this cake");
            }

            var cake = await _dbContext.Cakes.Where(x => x.Id == cakeId).ProjectTo<CakeDto>().FirstOrDefaultAsync();
            var member = await _dbContext.Members.Where(x => x.Id == memberId).ProjectTo<MemberDto>().FirstOrDefaultAsync();

            if (member == null)
            {
                return BadRequest("Member not found");
            }

            if (cake == null)
            {
                return BadRequest("Cake not found");
            }

            await _voteService.Vote(memberId, cakeId, grade, participated);

            return RedirectToAction(nameof(Thanks), new { cakeId = cake.Id });
        }

        [HttpGet("/vote")]
        [AllowAnonymous]
        public async Task<IActionResult> VoteAnonymous(int cakeId, int memberId, string token)
        {
            var vm = new VoteViewModel();

            if (!_voteService.Validate(memberId, cakeId, token))
            {
                ModelState.AddModelError("", "The provided token is invalid");

                return View(nameof(Vote), vm);
            }

            if (await _dbContext.Votes.AnyAsync(x => x.Member.Id == memberId && x.Cake.Id == cakeId))
            {
                ModelState.AddModelError("", "You have already voted for this cake");

                return View(nameof(Vote), vm);
            }

            var cake = await _dbContext.Cakes
                .Where(x => x.Id == cakeId)
                .ProjectTo<CakeDto>()
                .FirstOrDefaultAsync();
            
            var member = await _dbContext.Members
                .Where(x => x.Id == memberId)
                .ProjectTo<MemberDto>()
                .FirstOrDefaultAsync();

            if (member == null)
            {
                ModelState.AddModelError("", "User not found");
            }

            if (cake == null)
            {
                ModelState.AddModelError("", "Cake not found");
            }

            vm.Member = member;
            vm.Cake = cake;
            vm.CakeId = cake.Id;
            vm.MemberId = member.Id;
            vm.Token = token;

            return View(nameof(Vote), vm);
        }

        [HttpPost("{cakeId}/vote")]
        [AllowAnonymous]
        public async Task<IActionResult> SubmitVote(VoteViewModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new Exception("Invalid model state");
            }

            if (await _dbContext.Votes.AnyAsync(x => x.Member.Id == model.MemberId && x.Cake.Id == model.CakeId))
            {
                ModelState.AddModelError("", "You have already voted for this cake");

                return View("Vote", new VoteViewModel());
            }

            if (User.Identity.IsAuthenticated)
            {
                await _voteService.Vote(model.MemberId, model.CakeId, model.Grade, model.Participated);
            }
            else 
            {
                await _voteService.Vote(model.MemberId, model.CakeId, model.Token, model.Grade, model.Participated);
            }

            return RedirectToAction(nameof(Cake), new {cakeId = model.CakeId});
        }

        [Route("/Thanks")]
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Thanks(int cakeId)
        {
            return View(cakeId);
        }
    }
}