﻿using AutoMapper.QueryableExtensions;
using CakeClub.Infrastructure;
using CakeClub.Infrastructure.Services;
using CakeClub.Models;
using CakeClub.Models.HomeViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace MvcApp.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly CakeService _cakeService;
        private readonly EmailService _emailService;

        public HomeController(ApplicationDbContext dbContext, CakeService cakeService, EmailService emailService)
        {
            _dbContext = dbContext;
            _cakeService = cakeService;
            _emailService = emailService;
        }

        public IActionResult Index()
        {
            return View();
        }
        
        public async Task<IActionResult> Upcoming()
        {
            var vm = new UpcomingViewModel();

            vm.Members = await _dbContext.Members.Where(x => x.Active).ProjectTo<MemberDto>().ToListAsync();
            vm.Weeks = await _cakeService.GetWeeks();
            
            return View(vm);
        }

        [HttpPost(Name = "SkipWeek")]
        public async Task<IActionResult> SkipWeek([FromForm] int weekId)
        {
            await _cakeService.SkipWeek(weekId);

            return RedirectToAction(nameof(Upcoming));
        }


        [HttpPost]
        public async Task<IActionResult> RemoveWeek([FromForm] int weekId)
        {
            await _cakeService.RemoveWeek(weekId);

            return RedirectToAction(nameof(Upcoming));
        }

        [HttpGet]
        public async Task<IActionResult> DequeueWeek()
        {
            await _cakeService.NextWeek();

            return RedirectToAction(nameof(Upcoming));
        }

        [HttpPost(Name = "UpdateWeek")]
        public async Task<IActionResult> UpdateWeek([FromForm] int weekId, [FromForm] int selectedMemberId)
        {
            await _cakeService.UpdateWeek(weekId, selectedMemberId);

            return RedirectToAction(nameof(Upcoming));
        }

        [HttpPost]
        public async Task<IActionResult> SendReminder([FromForm] int weekId)
        {
            var week = await _cakeService.GetWeek(weekId);

            var member = await _dbContext.Members.ProjectTo<MemberDto>().FirstOrDefaultAsync(x => x.Id == week.BakerId);

            if (!member.Active || string.IsNullOrEmpty(member.UserEmail)) return Ok();

            await _emailService.SendReminderEmail(member, week.StartDate);

            await _cakeService.IncrementRemindersSent(weekId);

            return RedirectToAction(nameof(Upcoming));
        }

        [HttpPost]
        public async Task<IActionResult> SwitchWithNextWeek([FromForm] int weekId)
        {
            await _cakeService.SwitchWithNextWeek(weekId);

            return RedirectToAction(nameof(Upcoming));
        }

        [HttpPost]
        public async Task<IActionResult> SwitchWithPreviousWeek([FromForm] int weekId)
        {
            await _cakeService.SwitchWithPreviousWeek(weekId);

            return RedirectToAction(nameof(Upcoming));
        }
    }
}
