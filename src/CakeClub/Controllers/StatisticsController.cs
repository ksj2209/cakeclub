using System;
using System.Linq;
using System.Threading.Tasks;
using CakeClub.Entities;
using CakeClub.Infrastructure;
using CakeClub.Models.StatisticsViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using CakeClub.Models;
using System.Collections.Generic;
using AutoMapper.QueryableExtensions;

namespace CakeClub.Controllers
{
    [Authorize]
    public class StatisticsController : Controller
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<User> _userManager;
        private readonly ILogger<StatisticsController> _logger;

        public StatisticsController (UserManager<User> userManager, ILogger<StatisticsController> logger, ApplicationDbContext dbContext)
        {
            _userManager = userManager;
            _logger = logger;
            _dbContext = dbContext;
        }

        public async Task<IActionResult> Index()
        {
            var cakesWithGrade = await _dbContext.Cakes
                .ProjectTo<CakeDto>()
                .ToListAsync();

            var members = await _dbContext.Members.ToListAsync();

            int totalWeeks = 0;
            if (cakesWithGrade.Any())
            {
                var firstCake = cakesWithGrade.Min(x => x.EventTime);
                var hoursSinceFirstCake = (DateTime.Now - firstCake).TotalHours;
                totalWeeks = Convert.ToInt32(Math.Floor(hoursSinceFirstCake / 24 / 7));
            }

            var currentRankings = (from c in cakesWithGrade
                                   where c.CreatorActive && c.Grade.HasValue
                                   group c by c.CreatorId into g
                                   select g.OrderByDescending(x => x.EventTime).FirstOrDefault())
                                   .OrderByDescending(cake => cake.Grade)
                                   .ToList();

            var model = new IndexViewModel
            {
                ActiveMembers = members.Count(member => member.Active),
                TotalMembers = members.Count,
                TotalCakes = cakesWithGrade.Count,
                TotalWeeks = Convert.ToInt32(totalWeeks),
                RankedCakes = cakesWithGrade.Count(cake => cake.Grade.HasValue),
                AverageCakeGrade = cakesWithGrade.Average(cake => cake.Grade),
                TopCakeGrade = cakesWithGrade.Max(cake => cake.Grade),
                LowestCakeGrade = cakesWithGrade.Min(cake => cake.Grade),
                CurrentRankings = currentRankings
            };

            return View(model);
        }

        public async Task<IActionResult> AllCakes()
        {
            var allCakes = await _dbContext.Cakes
                .ProjectTo<CakeDto>()
                .ToListAsync();

            return View(allCakes);
        }
    }
}