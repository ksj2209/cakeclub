using CakeClub.Entities;
using CakeClub.Infrastructure;
using CsvHelper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;

namespace CakeClub
{
    class MemberEntry
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Initials { get; set; }
        public DateTime JoinDate { get; set; }
        public DateTime? DepartDate { get; set; }
        public string DepartReason { get; set; }
        public bool Active { get; set; }
        public int Seq { get; set; }
        public string Email { get; set; }
        public bool IsAdmin { get; set; }
    }

    class CakeEntry
    {
        public int Id { get; set; }
        public DateTime BakeDate { get; set; }
        public int MemberId { get; set; }
        public string Description { get; set; }
        public bool BakeSelf { get; set; }
        //public string Comments { get; set; }
        public bool IsCake { get; set; }
    }

    class VoteEntry
    {
        public int CakeId { get; set; }
        public int MemberId { get; set; }
        public int? Grade { get; set; }
        public bool Participated { get; set; }
    }

    static class DbInitializerMiddleware
    {
        public static void ImportData(this IApplicationBuilder app, IHostingEnvironment env)
        {
            var loggerFactory = new LoggerFactory();
            loggerFactory.AddFilter(new Dictionary<string, LogLevel>
            {
                { "Microsoft", LogLevel.Warning },
                { "System", LogLevel.Error }
            });
            loggerFactory.AddConsole();

            var logger = loggerFactory.CreateLogger("Seed");

            var cakeMapping = new Dictionary<int, Cake>();
            var memberMapping = new Dictionary<int, Member>();

            var dbContext = app.ApplicationServices.GetService<ApplicationDbContext>();

            // Run migrations
            dbContext.Database.Migrate();
            
            // Only seed the database on the first run
            if (dbContext.Users.Any())
            {
                return;
            }

            var userManager = app.ApplicationServices.GetService<UserManager<User>>();
            var roleManager = app.ApplicationServices.GetService<RoleManager<Role>>();

            // Create admin role and user
            roleManager.CreateAsync(new Role("Administrator")).GetAwaiter().GetResult();

            using (var reader = File.OpenText("../../data/members.txt"))
            {
                using (var csv = new CsvReader(reader))
                {
                    csv.Configuration.Delimiter = ";";

                    while (csv.Read())
                    {
                        var member = csv.GetRecord<MemberEntry>();
                        if (!string.IsNullOrEmpty(member.Email))
                        {
                            var user = new User
                            {
                                UserName = member.Email,
                                Email = member.Email,
                                EmailConfirmed = true
                            };

                            IdentityResult result;
                            if (env.IsDevelopment())
                                result = userManager.CreateAsync(user, "ILoveCake123!").GetAwaiter().GetResult();
                            else
                                result = userManager.CreateAsync(user).GetAwaiter().GetResult();

                            if (member.IsAdmin)
                            {
                                user = userManager.FindByNameAsync(member.Email).GetAwaiter().GetResult();

                                userManager.AddToRoleAsync(user, "Administrator").GetAwaiter().GetResult();
                            }

                            logger.LogTrace($"Created user {member.Email}");
                        }
                    }
                }
            }

            using (var reader = File.OpenText("../../data/members.txt"))
            {
                using (var csv = new CsvReader(reader))
                {
                    csv.Configuration.Delimiter = ";";
                    while (csv.Read())
                    {
                        var member = csv.GetRecord<MemberEntry>();
                        
                        logger.LogTrace(JsonConvert.SerializeObject(member));

                        var entity = new Member
                        {
                            FirstName = member.Name.Split(' ')[0],
                            LastName = string.Join(" ", member.Name.Split(' ').Skip(1)),
                            Joined = member.JoinDate,
                            Departed = member.DepartDate,
                            DepartedReason = member.DepartReason,
                            Active = member.Active
                        };

                        if (!string.IsNullOrEmpty(member.Email))
                        {
                            var user = dbContext.Users.FirstOrDefault(x => x.Email == member.Email);

                            entity.User = user;
                        }

                        dbContext.Members.Add(entity);

                        memberMapping.Add(member.Id, entity);
                    }
                }
            }

            using (var reader = File.OpenText("../../data/cakes.txt"))
            {
                using (var csv = new CsvReader(reader))
                {
                    csv.Configuration.Delimiter = ";";

                    while (csv.Read())
                    {
                        var cake = csv.GetRecord<CakeEntry>();

                        if (!cake.IsCake) continue;
                        
                        logger.LogTrace(JsonConvert.SerializeObject(cake));

                        var member = memberMapping[cake.MemberId];

                        var entity = new Cake
                        {
                            EventTime = cake.BakeDate,
                            Creator = member,
                            Name = cake.Description,
                            BakeSelf = cake.BakeSelf,
                            VotesSent = true
                        };

                        dbContext.Cakes.Add(entity);

                        cakeMapping.Add(cake.Id, entity);
                    }
                }
            }

            using (var reader = File.OpenText("../../data/votes.txt"))
            {
                using (var csv = new CsvReader(reader))
                {
                    csv.Configuration.Delimiter = ";";

                    while (csv.Read())
                    {
                        var vote = csv.GetRecord<VoteEntry>();
                        
                        var member = memberMapping[vote.MemberId];
                        Cake cake;
                        
                        if (!cakeMapping.TryGetValue(vote.CakeId, out cake))
                        {
                            continue;
                        }

                        var entity = new Vote
                        {
                            Cake = cake,
                            Member = member,
                            Grade = vote.Participated ? vote.Grade : null,
                            Participated = vote.Participated
                        };

                        dbContext.Votes.Add(entity);
                    }

                }
            }

            dbContext.SaveChanges();

            var date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var previousMonday = date.Subtract(TimeSpan.FromDays((6 + (int)DateTime.Now.DayOfWeek) % 7));
            var nextMonday = previousMonday.Add(TimeSpan.FromDays(7));

            foreach (var member in dbContext.Members.Where(x => x.Active))
            {
                dbContext.Weeks.Add(new Week
                {
                    Baker = member,
                    StartDate = nextMonday
                });

                nextMonday = nextMonday.Add(TimeSpan.FromDays(7));
            }

            dbContext.SaveChanges();
        }
    }
}