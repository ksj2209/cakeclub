﻿using CakeClub.Models;
using System;

namespace CakeClub.Emails
{
    public class CakeInviteEmail
    {
        public CakeDto Cake { get; set; }
        public MemberDto Member { get; set; }
        public DateTime Time { get; set; }
    }
}
