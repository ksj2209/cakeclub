namespace CakeClub.Emails
{
    public class ForgotPasswordEmail 
    {
        public string Callback { get; set; }

        public static string Template
        {
            get
            {
                return @"
                    @model CakeClub.Emails.ForgotPasswordEmailModel
                    <h1>Forgot Password?</h1>
                    <p>Please reset your password by clicking here: <a href='@Model.Callback'>Reset Password</a></p>
                ";
            }
        }
    }
}