﻿using CakeClub.Models;
using System;

namespace CakeClub.Emails
{
    public class ReminderEmail
    {
        public MemberDto Member { get; set; }
        public int Week { get; set; }
    }
}
