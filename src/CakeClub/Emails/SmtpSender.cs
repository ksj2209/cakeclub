using System.IO;
using System.Threading;
using System.Threading.Tasks;
using FluentEmail.Core;
using FluentEmail.Core.Interfaces;
using FluentEmail.Core.Models;
using MailKit.Net.Smtp;
using MimeKit;

namespace CakeClub.Emails
{
    public class SmtpSender : ISender
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }

        public SmtpSender(string username, string password, string host, int port)
        {
            UserName = username;
            Password = password;
            Host = host;
            Port = port;
        }

        public SendResponse Send(Email email, CancellationToken? token = default(CancellationToken?))
        {
            return SendAsync(email, token).GetAwaiter().GetResult();
        }

        public async Task<SendResponse> SendAsync(Email email, CancellationToken? token = default(CancellationToken?))
        {
            var message = new MimeMessage();

            message.Subject = email.Data.Subject;
            message.From.Add(new MailboxAddress(email.Data.FromAddress.Name, email.Data.FromAddress.EmailAddress));

            foreach (var address in email.Data.ReplyToAddresses)
            {
                message.ReplyTo.Add(new MailboxAddress(address.Name, address.EmailAddress));
            }

            foreach (var to in email.Data.ToAddresses)
            {
                message.To.Add(new MailboxAddress(to.Name, to.EmailAddress));
            }

            var bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = email.Data.Body;

            foreach (var attachment in email.Data.Attachments)
            {
                using (var ms = new MemoryStream())
                {
                    attachment.Data.CopyTo(ms);
                
                    var contentTypeParts = attachment.ContentType.Split('/');

                    bodyBuilder.Attachments.Add(attachment.Filename, ms.ToArray(), new ContentType(contentTypeParts[0], contentTypeParts[1]));
                }
            }

            message.Body = bodyBuilder.ToMessageBody();

            using (var client = new SmtpClient())
            {
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                await client.ConnectAsync(Host, Port, false);

                await client.AuthenticateAsync(UserName, Password);

                await client.SendAsync(message);

                await client.DisconnectAsync(true);
            }

            return new SendResponse();
        }
    }
}