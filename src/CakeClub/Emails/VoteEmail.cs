using CakeClub.Models;

namespace CakeClub.Emails
{
    public class VoteEmail
    {
        public CakeDto Cake { get; set; }
        public MemberDto Member { get; set; }
        public string VoteCallback { get; set; }
        public string DirectVoteCallback { get; set; }
    }
}