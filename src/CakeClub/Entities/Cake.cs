using System;
using System.Collections.Generic;

namespace CakeClub.Entities
{
    public class Cake
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Recipe { get; set; }
        public bool BakeSelf { get; set; }
        public bool VotesSent { get; set; }
        public DateTime EventTime { get; set; }
        public int CreatorId { get; set; }
        public Member Creator { get; set; }
        public ICollection<Vote> Votes { get; set; } = new List<Vote>();
        
        public static bool operator == (Cake rhs, Cake lhs)
        {
            return object.Equals(lhs, rhs);
        }

        public static bool operator != (Cake rhs, Cake lhs)
        {
            return !object.Equals(lhs, rhs);
        }

        public override bool Equals (object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            
            if (ReferenceEquals(obj, this))
                return true;

            var rhs = obj as Cake;
            return Id == rhs.Id;
        }
        
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}