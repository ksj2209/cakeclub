using System;
using System.Collections.Generic;

namespace CakeClub.Entities
{
    public class Member
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Active { get; set; }
        public DateTime Joined { get; set; }
        public DateTime? Departed { get; set; }
        public string DepartedReason { get; set; }
        public int AdditionalCakesBaked { get; set; } // Number of additional cakes which has been baked by this user outside of the normal routine
        public ICollection<Cake> Cakes { get; set; } = new List<Cake>();
        public ICollection<Vote> Votes { get; set; } = new List<Vote>();
        public ICollection<Week> Weeks { get; set; } = new List<Week>();

        public int? UserId { get; set; }
        public User User { get; set; }

        public static bool operator == (Member rhs, Member lhs)
        {
            return object.Equals(lhs, rhs);
        }

        public static bool operator != (Member rhs, Member lhs)
        {
            return !object.Equals(lhs, rhs);
        }

        public override bool Equals (object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            
            if (ReferenceEquals(obj, this))
                return true;

            var rhs = obj as Member;
            return Id == rhs.Id;
        }
        
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}