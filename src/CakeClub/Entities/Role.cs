using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace CakeClub.Entities
{
    public class Role : IdentityRole<int>
    {
        public Role() : base(){}
        public Role(string roleName) : base()
        {
            Name = roleName;
        }
        
        public static bool operator == (Role rhs, Role lhs)
        {
            return object.Equals(lhs, rhs);
        }

        public static bool operator != (Role rhs, Role lhs)
        {
            return !object.Equals(lhs, rhs);
        }

        public override bool Equals (object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            
            if (ReferenceEquals(obj, this))
                return true;

            var rhs = obj as Role;
            return Id == rhs.Id;
        }
        
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
