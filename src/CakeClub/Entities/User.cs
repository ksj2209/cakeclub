﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace CakeClub.Entities
{
    public class User : IdentityUser<int>
    {
        public Member Member { get; set; }

        public static bool operator ==(User rhs, User lhs)
        {
            return object.Equals(lhs, rhs);
        }

        public static bool operator !=(User rhs, User lhs)
        {
            return !object.Equals(lhs, rhs);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            if (ReferenceEquals(obj, this))
                return true;

            var rhs = obj as User;
            return Id == rhs.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
