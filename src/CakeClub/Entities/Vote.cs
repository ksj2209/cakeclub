namespace CakeClub.Entities 
{
    public class Vote
    {  
        public int Id { get; set; }
        public Member Member { get; set; }
        public Cake Cake { get; set; }
        public int? Grade { get; set; }
        public bool Participated { get; set; }
        
        public static bool operator == (Vote rhs, Vote lhs)
        {
            return object.Equals(lhs, rhs);
        }

        public static bool operator != (Vote rhs, Vote lhs)
        {
            return !object.Equals(lhs, rhs);
        }

        public override bool Equals (object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            
            if (ReferenceEquals(obj, this))
                return true;

            var rhs = obj as Vote;
            return Id == rhs.Id;
        }
        
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}