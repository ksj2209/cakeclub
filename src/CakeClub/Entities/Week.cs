using System;

namespace CakeClub.Entities
{
    /// <summary>
    /// Represents a week where a user is selected to bake a cake
    /// </summary>
    public class Week
    {
        public int Id { get; set; }

        /// <summary>
        /// First day of the week (monday)
        /// </summary>
        public DateTime StartDate { get; set; }
        public Member Baker { get; set; }
        public bool HasBeenSkipped { get; set; } = false; // Should this week be skipped in the routine?
        public bool IsRoutineBaker { get; set; } = true; // Is this the baker which has to bake as part of the routine?
        public int RemindersSent { get; set; }

        public static bool operator == (Week rhs, Week lhs)
        {
            return object.Equals(lhs, rhs);
        }

        public static bool operator != (Week rhs, Week lhs)
        {
            return !object.Equals(lhs, rhs);
        }

        public override bool Equals (object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            
            if (ReferenceEquals(obj, this))
                return true;

            var rhs = obj as Week;
            return Id == rhs.Id;
        }
        
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}