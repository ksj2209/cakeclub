using CakeClub.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CakeClub.Infrastructure
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, int>
    {
        public DbSet<Member> Members { get; set; }
        public DbSet<Cake> Cakes { get; set; }
        public DbSet<Vote> Votes { get; set; }
        public DbSet<Week> Weeks { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {}

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<IdentityUserRole<int>>(i => {
                i.ToTable("UserRole");
                i.HasKey(x => new { x.RoleId, x.UserId });
            });
            builder.Entity<IdentityUserLogin<int>>(i => {
                i.ToTable("UserLogin");
                i.HasKey(x => new { x.ProviderKey, x.LoginProvider });
            });
            builder.Entity<IdentityRoleClaim<int>>(i => {
                i.ToTable("RoleClaims");
                i.HasKey(x => x.Id);
            });
            builder.Entity<IdentityUserClaim<int>>(i => {
                i.ToTable("UserClaims");
                i.HasKey(x => x.Id);
            });

            builder.Entity<User>(e =>
            {
                e.ToTable("Users");
            });

            builder.Entity<Role>(e =>
            {
                e.ToTable("Roles");
            });

            builder.Entity<Member>(e =>
            {
                e.HasKey(t => t.Id);

                e.Property(t => t.Active).IsRequired();
                e.Property(t => t.FirstName).HasMaxLength(500);
                e.Property(t => t.LastName).HasMaxLength(500);
                e.Property(t => t.Joined).IsRequired();
                e.Property(t => t.AdditionalCakesBaked).IsRequired().HasDefaultValue(0);

                e.HasOne(t => t.User).WithOne(t => t.Member).HasForeignKey<Member>(t => t.UserId);
                e.HasMany(t => t.Cakes).WithOne(t => t.Creator).HasForeignKey(t => t.CreatorId).IsRequired();
                e.HasMany(t => t.Votes).WithOne(t => t.Member).IsRequired();
            });

            builder.Entity<Cake>(e => 
            {
                e.HasKey(t => t.Id);
                e.Property(t => t.Id).ValueGeneratedOnAdd();

                e.Property(t => t.Name).IsRequired();
                e.Property(t => t.BakeSelf).IsRequired();
                e.Property(t => t.VotesSent).IsRequired();
                
                e.HasMany(t => t.Votes).WithOne(t => t.Cake).IsRequired();
            });

            builder.Entity<Vote>(e =>
            {
                e.HasKey(t => t.Id);
                e.Property(t => t.Id).ValueGeneratedOnAdd();

                e.Property(t => t.Participated).IsRequired();
            });

            builder.Entity<Week>(e =>
            {
                e.HasKey(t => t.Id);
                e.Property(t => t.Id).ValueGeneratedOnAdd();

                e.Property(t => t.StartDate).IsRequired();
                e.Property(t => t.HasBeenSkipped).IsRequired();

                e.HasOne(t => t.Baker).WithMany(t => t.Weeks);
            });
        }
    }
}
