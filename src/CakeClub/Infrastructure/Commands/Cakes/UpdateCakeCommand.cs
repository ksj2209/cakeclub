﻿using System;

namespace CakeClub.Infrastructure.Commands.Cakes
{
    public class UpdateCakeCommand
    {
        public int CakeId { get; set; }
        public string Name { get; set; }
        public DateTime When { get; set; }
        public string Recipe { get; set; }
    }
}
