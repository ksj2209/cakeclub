﻿using AutoMapper.QueryableExtensions;
using CakeClub.Entities;
using CakeClub.Infrastructure.Commands.Cakes;
using CakeClub.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CakeClub.Infrastructure.Services
{
    public class CakeService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger<CakeService> _logger;
        private readonly DbContextOptions<ApplicationDbContext> _dbContextOptions;
        private readonly UserProvider _userProvider;
        private readonly UserService _userService;

        public CakeService(ApplicationDbContext dbContext, ILogger<CakeService> logger, DbContextOptions<ApplicationDbContext> dbContextOptions, UserProvider userProvider, UserService userService)
        {
            _dbContext = dbContext;
            _logger = logger;
            _dbContextOptions = dbContextOptions;
            _userProvider = userProvider;
            _userService = userService;
        }

        /// <summary>
        /// Create a new cake
        /// </summary>
        /// <param name="name"></param>
        /// <param name="when"></param>
        /// <returns></returns>
        public async Task<int> CreateCake(string name, DateTime when, string recipe, bool bakeSelf, bool votesSent, int? memberId = null)
        {
            MemberDto member;
            if (memberId.HasValue && _userProvider.IsAdministrator())
                member = await _userService.GetMember(memberId.Value);
            else 
                member = await _userProvider.GetCurrentMember();

            if (member == null) throw new Exception("User not found");

            var cake = new Cake
            {
                Name = name,
                Recipe = recipe,
                EventTime = when,
                CreatorId = member.Id,
                BakeSelf = bakeSelf, 
                VotesSent = votesSent
            };

            _dbContext.Cakes.Add(cake);

            await _dbContext.SaveChangesAsync();

            return cake.Id;
        }

        /// <summary>
        /// Get all cakes
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<CakeDto>> GetCakes()
        {
            return await _dbContext.Cakes.ProjectTo<CakeDto>().ToListAsync();
        }

        /// <summary>
        /// Get a cake by its id
        /// </summary>
        /// <param name="cakeId"></param>
        /// <returns></returns>
        public async Task<CakeDto> GetCake(int cakeId)
        {
            return await _dbContext.Cakes.Where(x => x.Id == cakeId).ProjectTo<CakeDto>().FirstOrDefaultAsync();
        }

        /// <summary>
        /// Get votes for a cake by its cake id
        /// </summary>
        /// <param name="cakeId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<VoteDto>> GetVotes(int cakeId)
        {
            return await _dbContext.Votes.Where(x => x.Cake.Id == cakeId).ProjectTo<VoteDto>().ToListAsync();
        }

        /// <summary>
        /// Get a list of all weeks
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<WeekDto>> GetWeeks()
        {
            var weeks = await _dbContext.Weeks
                .ProjectTo<WeekDto>()
                .ToListAsync();

            foreach (var week in weeks)
            {
                week.IsEventCreated = await _dbContext.Cakes.AnyAsync(x => x.CreatorId == week.BakerId && x.EventTime >= week.StartDate && x.EventTime < week.StartDate.AddDays(7));
            }

            return weeks;
        }

        /// <summary>
        /// Get a week by its id
        /// </summary>
        /// <param name="weekId"></param>
        /// <returns></returns>
        public async Task<WeekDto> GetWeek(int weekId)
        {
            return await _dbContext.Weeks.Where(x => x.Id == weekId).ProjectTo<WeekDto>().FirstOrDefaultAsync();
        }

        /// <summary>
        /// Get current week
        /// </summary>
        /// <returns></returns>
        public async Task<WeekDto> GetNextWeek()
        {
            var week = await _dbContext.Weeks
                .OrderByDescending(x => x.StartDate)
                .ProjectTo<WeekDto>()
                .FirstOrDefaultAsync();

            week.IsEventCreated = await _dbContext.Cakes.AnyAsync(x => x.CreatorId == week.BakerId && x.EventTime >= week.StartDate && x.EventTime < week.StartDate.AddDays(7));

            return week;
        }

        /// <summary>
        /// Update cake info
        /// </summary>
        /// <param name="cakeId"></param>
        /// <param name="name"></param>
        /// <param name="when"></param>
        /// <param name="recipe"></param>
        /// <returns></returns>
        public async Task<int> UpdateCake(int id, string name, DateTime when, string recipe, bool bakeSelf, int? memberId = null)
        {
            var cake = await _dbContext.Cakes.FirstOrDefaultAsync(x => x.Id == id);

            if (cake == null)
            {
                throw new Exception("Cake does not exist");
            }

            var currentMember = await _userProvider.GetCurrentMember();

            if (!_userProvider.IsAdministrator() && currentMember.Id != cake.CreatorId)
            {
                throw new Exception("You do not have permission to edit this cake");
            }

            MemberDto member = currentMember;
            if (memberId.HasValue && _userProvider.IsAdministrator())
                member = await _userService.GetMember(memberId.Value);

            if (member == null) throw new Exception("User not found");

            cake.Name = name;
            cake.Recipe = recipe;
            cake.EventTime = when;
            cake.CreatorId = member.Id;
            cake.BakeSelf = bakeSelf; 

            await _dbContext.SaveChangesAsync();

            return cake.Id;
        }

        /// <summary>
        /// Delete a cake and all related votes
        /// </summary>
        /// <param name="cakeId"></param>
        /// <returns></returns>
        public async Task DeleteCake(int cakeId)
        {
            var cake = await _dbContext.Cakes
                .FirstOrDefaultAsync(x => x.Id == cakeId);
            
            var votes = await _dbContext.Votes
                .Where(x => x.Cake.Id == cakeId)
                .ToListAsync();

            _dbContext.Cakes.Remove(cake);
            _dbContext.Votes.RemoveRange(votes);

            await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Skip a week and clear its properties (Baker and IsRoutineBake)
        /// </summary>
        /// <param name="weekId">The id of the week</param>
        public async Task SkipWeek(int weekId)
        {
            var week = await _dbContext.Weeks.Include(x => x.Baker).FirstOrDefaultAsync(x => x.Id == weekId);
            var baker = await _dbContext.Members.FirstOrDefaultAsync(x => x.Id == week.Baker.Id);

            // Update existing weeks after the skipped week
            foreach (var week2 in _dbContext.Weeks.Where(x => x.StartDate > week.StartDate))
            {
                week2.StartDate = week2.StartDate.AddDays(7);
            }

            // Assign baker to the week after
            var newWeek = new Week
            {
                Baker = baker,
                StartDate = week.StartDate.AddDays(7),
                HasBeenSkipped = false,
                IsRoutineBaker = week.IsRoutineBaker // Keep the routine bake flag
            };

            _dbContext.Weeks.Add(newWeek);

            // Update the skipped week
            week.HasBeenSkipped = true;
            week.Baker = null;
            week.IsRoutineBaker = false;

            await _dbContext.SaveChangesAsync();
        }

         /// <summary>
        /// Switch two weeks
        /// </summary>
        /// <param name="weekId">The id of the first week</param>
        /// <param name="weekId2">The id of the second week</param>
        public async Task SwitchWithNextWeek(int weekId)
        {
            var weeks = await _dbContext.Weeks
                .OrderBy(x => x.StartDate)
                .ToListAsync();

            weeks = weeks.SkipWhile(x => x.Id != weekId).Take(2).ToList();

            var week = weeks.FirstOrDefault();
            var week2 = weeks.LastOrDefault();

            if (week == null || week2 == null)
            {
                throw new Exception("Cannot switch weeks that does not exist");
            }

            var tempWeekDate = week.StartDate;

            week.StartDate = week2.StartDate;
            week2.StartDate = tempWeekDate;

            await _dbContext.SaveChangesAsync();
        }

                 /// <summary>
        /// Switch two weeks
        /// </summary>
        /// <param name="weekId">The id of the first week</param>
        /// <param name="weekId2">The id of the second week</param>
        public async Task SwitchWithPreviousWeek(int weekId)
        {
            var weeks = await _dbContext.Weeks
                .OrderByDescending(x => x.StartDate)
                .ToListAsync();

            weeks = weeks.SkipWhile(x => x.Id != weekId).Take(2).ToList();

            var week = weeks.FirstOrDefault();
            var week2 = weeks.LastOrDefault();

            if (week == null || week2 == null)
            {
                throw new Exception("Cannot switch weeks that does not exist");
            }

            var tempWeekDate = week.StartDate;

            week.StartDate = week2.StartDate;
            week2.StartDate = tempWeekDate;

            await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Remove a week without requeueing the baker
        /// Shift all weeks after to one week prior
        /// </summary>
        /// <param name="weekId">The id of the week</param>
        public async Task RemoveWeek(int weekId)
        {
            var weekToRemove = await _dbContext.Weeks
                .FirstOrDefaultAsync(x => x.Id == weekId);

            var weeksAfter = await _dbContext.Weeks
                .Where(x => x.StartDate > weekToRemove.StartDate)
                .OrderBy(x => x.StartDate)
                .ToListAsync();

            _dbContext.Weeks.Remove(weekToRemove);

            // Start shifting weeks
            int skippedWeeks = 0;
            foreach (var week in weeksAfter)
            {
                // Do not skift skipped weeks
                if (week.HasBeenSkipped)
                {
                    skippedWeeks++;
                    continue;
                }

                // If this is not a routine baker there is probably a reason he has been placed here
                if (!week.IsRoutineBaker)
                {
                    skippedWeeks++;
                    continue;  
                }

                week.StartDate = week.StartDate.Subtract(TimeSpan.FromDays(7 * (skippedWeeks + 1)));
                skippedWeeks = 0;
            }

            await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Update a week with a new baker
        /// Will also advance existing after the selected week by one week
        /// </summary>
        /// <param name="weekId">The id of the week</param>
        /// <param name="bakerId">The id of the new baker</param>
        public async Task UpdateWeek(int weekId, int bakerId)
        {
            var week = await _dbContext.Weeks.Include(x => x.Baker).FirstOrDefaultAsync(x => x.Id == weekId);
            var baker = await _dbContext.Members.FirstOrDefaultAsync(x => x.Id == bakerId);

            // Pointless
            if (week.Baker == baker) return;

            // Increment the addional cakes of the baker
            baker.AdditionalCakesBaked++;

            if (week.HasBeenSkipped)
            {
                week.Baker = baker;
            }
            else
            {
                var originalStartDate = week.StartDate;

                var followingWeeks = await _dbContext.Weeks
                    .Where(x => x.StartDate >= week.StartDate)
                    .OrderByDescending(x => x.StartDate)
                    .ToListAsync();

                // Update weeks after
                var skippedWeeks = 0;
                foreach (var followingWeek in followingWeeks)
                {
                    // Do not move skipped weeks
                    if (followingWeek.HasBeenSkipped)
                    {
                        skippedWeeks++;
                        continue;
                    }

                    // Move the week
                    followingWeek.StartDate = followingWeek.StartDate.AddDays(7 * (skippedWeeks + 1));

                    // Reset skipped counter
                    skippedWeeks = 0;
                }

                // Add new week for baker
                var newWeek = new Week
                {
                    StartDate = originalStartDate,
                    Baker = baker,
                    IsRoutineBaker = false // This is not the usual baker for this week
                };

                _dbContext.Weeks.Add(newWeek);
            }

            await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Advance to the next valid week
        /// </summary>
        public async Task NextWeek()
        {
            var weeks = await _dbContext.Weeks
                .Include(x => x.Baker)
                .OrderBy(x => x.StartDate)
                .ToListAsync();

            // No weeks
            if (weeks.Count == 0) return;

            var currentWeek = weeks.First();

            // Create a new week if this was a routine bake
            if (currentWeek.IsRoutineBaker)
            {
                var baker = await _dbContext.Members.FirstOrDefaultAsync(x => x == currentWeek.Baker);

                var lastWeek = weeks.Last();

                var newWeek = new Week
                {
                    Baker = baker,
                    IsRoutineBaker = true,
                    StartDate = lastWeek.StartDate.AddDays(7)
                };

                _dbContext.Weeks.Add(newWeek);
            }

            // Remove the first week
            _dbContext.Weeks.Remove(currentWeek);

            // Select the next week
            int skipWeeks = 1;
            var nextWeek = weeks.Skip(skipWeeks).FirstOrDefault();

            // Loop until we have a valid next week
            while (true)
            {
                // If there are no more weeks
                if (nextWeek == null) break;

                // If this is a week that should be skipped
                if (nextWeek.HasBeenSkipped) break;

                // If the next week is not a routine bake it is valid no matter what
                if (!nextWeek.IsRoutineBaker)
                {
                    break;
                }

                // If the baker has baked any additional cakes he should be skipped
                if (nextWeek.Baker.AdditionalCakesBaked > 0)
                {
                    nextWeek.Baker.AdditionalCakesBaked--;

                    // Find the last week from the change tracker to account for objects not yet stored in the database
                    var lastWeek = _dbContext.ChangeTracker.Entries<Week>()
                        .OrderByDescending(x => x.Entity.StartDate)
                        .First();

                    // We still need to enqueue his next routine bake
                    var newWeek = new Week
                    {
                        Baker = nextWeek.Baker,
                        IsRoutineBaker = true,
                        StartDate = lastWeek.Entity.StartDate.AddDays(7)
                    };

                    _dbContext.Weeks.Add(newWeek);
                }
                else
                {
                    // If not it is his turn
                    break;
                }

                // Skip this week and choose the next
                _dbContext.Weeks.Remove(nextWeek);

                skipWeeks++;
                nextWeek = weeks.Skip(skipWeeks).FirstOrDefault();
            }

            await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Increment the number of reminders sent to the baker
        /// </summary>
        /// <param name="weekId"></param>
        /// <returns></returns>
        public async Task IncrementRemindersSent(int weekId)
        {
            var week = await _dbContext.Weeks.FirstOrDefaultAsync(x => x.Id == weekId);

            week.RemindersSent++;

            await _dbContext.SaveChangesAsync();
        }
    }
}
