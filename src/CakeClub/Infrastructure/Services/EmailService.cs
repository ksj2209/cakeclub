using AutoMapper.QueryableExtensions;
using CakeClub.Emails;
using CakeClub.Models;
using CakeClub.Options;
using FluentEmail.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace CakeClub.Infrastructure.Services
{
    public class EmailService
    {
        private readonly MailOptions _mailOptions;
        private readonly ILogger<EmailService> _logger;
        private readonly ApplicationDbContext _dbContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUrlHelperFactory _urlHelperFactory;
        private readonly IActionContextAccessor _actionContextAccessor;

        public EmailService(IOptions<MailOptions> options, ILogger<EmailService> logger, ApplicationDbContext dbContext, IHttpContextAccessor httpContextAccessor, IActionContextAccessor actionContextAccessor, IUrlHelperFactory urlHelperFactory)
        {
            _mailOptions = options.Value;
            _logger = logger;
            _dbContext = dbContext;
            _httpContextAccessor = httpContextAccessor;
            _urlHelperFactory = urlHelperFactory;
            _actionContextAccessor = actionContextAccessor;
        }

        private async Task<IEnumerable<MemberDto>> GetActiveMembers()
        {
            var activeUsers = await _dbContext.Members
                .Where(x => x.Active)
                .ProjectTo<MemberDto>()
                .ToListAsync();

            return activeUsers.Where(x => !string.IsNullOrEmpty(x.UserEmail)).ToList();
        }

        public async Task SendCakeInvites(CakeDto cake)
        {
            var activeMembers = await GetActiveMembers();

            var calendarAttachment = GetCalendarAttachment(cake, activeMembers);

            foreach (var user in activeMembers)
            {
                var body = $@"
                    <h1>Hi, {user.FirstName} {user.LastName}</h1>
                    <p>You are hereby invited to the Cake Club meeting on {cake.EventTime.ToString("dddd, MMMM dd, yyyy HH:mm")}</p>
                ";

                var response = await Email.From(_mailOptions.Sender)
                    .To(user.UserEmail)
                    .Subject(cake.Name)
                    .Body(body, true)
                    .Attach(calendarAttachment)
                    .SendAsync();

                if (response.ErrorMessages.Count > 0)
                {
                    foreach (var error in response.ErrorMessages)
                    {
                        _logger.LogError(error);
                    }
                }
            }
        }

        public async Task SendReminderEmail(MemberDto member, DateTime date)
        {
            var urlHelper = _urlHelperFactory.GetUrlHelper(_actionContextAccessor.ActionContext);
            var link = urlHelper.AbsoluteAction("Index", "Cakes");
                
            var week = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            var thisWeek = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(DateTime.UtcNow, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var model = new ReminderEmail { Member = member, Week = week };

            var body = $@"
                <h2>Hi, {member.FirstName} {member.LastName}</h2>
                <p>This is a friendly reminder that you have been assigned to bake a cake for week {week.ToString()}.</p>
                <p>YOu can create the invitation from the <a href=""{link}"">calendar page</a> by clicking <b>Create cake</b>.</p>
            ";

            if (week == thisWeek)
            {
                body = $@"
                    <h2>Hi, {member.FirstName} {member.LastName}</h2>
                    <p>This is a last reminder that you have been assigned to bake a cake for this week.</p>
                    <p>YOu can create the invitation from the <a href=""{link}"">calendar page</a> by clicking <b>Create cake</b>.</p>
                ";
            }

            var response = await Email
                .From(_mailOptions.Sender)
                .To(member.UserEmail)
                .Subject("Cake Club Reminder")
                .Body(body, true)
                .SendAsync();

            if (response.ErrorMessages.Count > 0)
            {
                foreach (var error in response.ErrorMessages)
                {
                    _logger.LogError(error);
                }
            }
        }

        public async Task SendVoteEmail(CakeDto cake, MemberDto member, string token)
        {
            var urlHelper = _urlHelperFactory.GetUrlHelper(_actionContextAccessor.ActionContext);
            var directVoteCallback = urlHelper.AbsoluteAction("VoteDirectly", "Cakes", new { memberId = member.Id, cakeId = cake.Id, token = token });
            var voteCallback = urlHelper.AbsoluteAction("Vote", "Cakes", new { memberId = member.Id, cakeId = cake.Id, token = token });

            var model = new VoteEmail { Cake = cake, Member = member, DirectVoteCallback = directVoteCallback, VoteCallback = voteCallback };

            var body = $@"
                <h1>Hi, {member.FirstName} {member.LastName}</h1>
                <p>Please, cast your vote for <b>{cake.Name}</b> by following <a href='{voteCallback}'>this link</a>, or by direct voting using one of the following links:</p>
                <p>or by direct voting using one of the following links:</p>
                <ul>
                    <li><a href='{directVoteCallback}&grade=1&participated=true'>Vote 1</a></li>
                    <li><a href='{directVoteCallback}&grade=2&participated=true'>Vote 2</a></li>
                    <li><a href='{directVoteCallback}&grade=3&participated=true'>Vote 3</a></li>
                    <li><a href='{directVoteCallback}&grade=4&participated=true'>Vote 4</a></li>
                    <li><a href='{directVoteCallback}&grade=5&participated=true'>Vote 5</a></li>
                    <li><a href='{directVoteCallback}&grade=6&participated=true'>Vote 6</a></li>
                    <li><a href='{directVoteCallback}&grade=7&participated=true'>Vote 7</a></li>
                    <li><a href='{directVoteCallback}&grade=8&participated=true'>Vote 8</a></li>
                    <li><a href='{directVoteCallback}&grade=9&participated=true'>Vote 9</a></li>
                    <li><a href='{directVoteCallback}&grade=10&participated=true'>Vote 10</a></li>
                    <li><a href='{directVoteCallback}&participated=true'>Participated</a></li>
                    <li><a href='{directVoteCallback}&participated=false'>Didn't participate</a></li>
                </ul>
            ";

            var response = await Email.From(_mailOptions.Sender)
                .To(member.UserEmail)
                .Subject($"Vote for {cake.Name}")
                .Body(body, true)
                .SendAsync();

            if (response.ErrorMessages.Count > 0)
            {
                foreach (var error in response.ErrorMessages)
                {
                    _logger.LogError(error);
                }
            }
        }

        public async Task SendForgotPasswordEmail(int userId, string email, string code)
        {
            var urlHelper = _urlHelperFactory.GetUrlHelper(_actionContextAccessor.ActionContext);
            var callbackUrl = urlHelper.AbsoluteAction("ResetPassword", "Account", new { userId = userId, code = code });

            var model = new ForgotPasswordEmail { Callback = callbackUrl };

            var body = $@"
                <h1>Forgot Password?</h1>
                <p>Please reset your password by clicking here: <a href='{callbackUrl}'>Reset Password</a></p>
            ";

            var response = await Email.From(_mailOptions.Sender)
                .To(email)
                .Subject($"Reset Password")
                .Body(body, true)
                .SendAsync();

            if (response.ErrorMessages.Count > 0)
            {
                foreach (var error in response.ErrorMessages)
                {
                    _logger.LogError(error);
                }
            }
        }

        public async Task SendWelcomeEmail(int userId, string code, string email)
        {
            var urlHelper = _urlHelperFactory.GetUrlHelper(_actionContextAccessor.ActionContext);
            var callbackUrl = urlHelper.AbsoluteAction("Setup", "Account", new { userId = userId, code = code });

            var body = $@"
                <p>You have been invited to join the prestigious Cake Club</p>
                <p>Visit <a href='{callbackUrl}'>this link</a> to accept the invitation and create your account.</p> 
            ";

            var response = await Email.From(_mailOptions.Sender)
                .To(email)
                .Subject($"Welcome to Cake Club")
                .Body(body, true)
                .SendAsync();

            if (response.ErrorMessages.Count > 0)
            {
                foreach (var error in response.ErrorMessages)
                {
                    _logger.LogError(error);
                }
            }
        }

        private FluentEmail.Core.Models.Attachment GetCalendarAttachment(CakeDto cake, IEnumerable<MemberDto> members)
        {
            var attachment = new FluentEmail.Core.Models.Attachment();

            StringBuilder sbCalendar = new StringBuilder();
            DateTime dtEnd = cake.EventTime.AddMinutes(20);

            sbCalendar.AppendLine("BEGIN:VCALENDAR");
            sbCalendar.AppendLine("VERSION:2.0");
            sbCalendar.AppendLine("PRODID:-//CakeClub");
            sbCalendar.AppendLine("CALSCALE:GREGORIAN");
            sbCalendar.AppendLine("METHOD:REQUEST");
            sbCalendar.AppendLine("BEGIN:VEVENT");
            sbCalendar.AppendLine("DTSTAMP:" + cake.EventTime.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z"));
            sbCalendar.AppendLine("DTSTART:" + cake.EventTime.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z"));
            sbCalendar.AppendLine("DTEND:" + dtEnd.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z"));
            sbCalendar.AppendLine("LOCATION:Sofa Group");
            sbCalendar.AppendLine("DESCRIPTION;ENCODING=QUOTED-PRINTABLE:CakeClub Invite");
            sbCalendar.AppendLine($"SUMMARY:{cake.Name}");
            sbCalendar.AppendLine("UID:" + Guid.NewGuid().ToString());
            sbCalendar.AppendLine("SEQUENCE:0");

            foreach (var attendee in members)
            {
                sbCalendar.AppendLine($"ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN=\"{attendee.UserEmail}\":MAILTO:{attendee.UserEmail}");
            }

            sbCalendar.AppendLine("CLASS:PUBLIC");
            sbCalendar.AppendLine("ORGANIZER:MAILTO:cakeclub@cakeclub.dk");
            sbCalendar.AppendLine("STATUS:TENTATIVE");
            sbCalendar.AppendLine("END:VEVENT");

            sbCalendar.AppendLine("BEGIN:VALARM");
            sbCalendar.AppendLine("TRIGGER:-PT30M");
            sbCalendar.AppendLine("REPEAT:2");
            sbCalendar.AppendLine("DURATION:PT15M");
            sbCalendar.AppendLine("ACTION:DISPLAY");
            sbCalendar.AppendLine("DESCRIPTION:CakeClub Invite");
            sbCalendar.AppendLine("END:VALARM");

            sbCalendar.AppendLine("END:VCALENDAR");

            byte[] bytes = Encoding.UTF8.GetBytes(sbCalendar.ToString());
            var stream = new MemoryStream(bytes);

            attachment.ContentType = "text/calendar";
            attachment.Data = stream;
            attachment.Filename = "event.ics";

            return attachment;
        }
    }
}