using AutoMapper.QueryableExtensions;
using CakeClub.Entities;
using CakeClub.Infrastructure.Commands.Cakes;
using CakeClub.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CakeClub.Infrastructure.Services
{
    public class UserProvider
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger<CakeService> _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserProvider(ApplicationDbContext dbContext, ILogger<CakeService> logger, IHttpContextAccessor httpContextAccessor)
        {
            _dbContext = dbContext;
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<MemberDto> GetCurrentMember()
        {
            return await _dbContext.Members
                .Where(x => x.User.UserName.Equals(_httpContextAccessor.HttpContext.User.Identity.Name, StringComparison.OrdinalIgnoreCase))
                .ProjectTo<MemberDto>()
                .FirstOrDefaultAsync();
        }

        public bool IsAdministrator()
        {
            return _httpContextAccessor.HttpContext.User.IsInRole("Administrator");
        }
    }
}
