﻿using AutoMapper.QueryableExtensions;
using CakeClub.Entities;
using CakeClub.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CakeClub.Infrastructure.Services
{
    public class UserService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<User> _userManager;
        private readonly EmailService _emailService;
        private readonly ILogger<UserService> _logger;

        public UserService(ApplicationDbContext dbContext, UserManager<User> userManager, EmailService emailService, ILogger<UserService> logger)
        {
            _dbContext = dbContext;
            _userManager = userManager;
            _emailService = emailService;
            _logger = logger;
        }

        public async Task<IEnumerable<MemberDto>> GetMembers()
        {
            return await _dbContext.Members
                .ProjectTo<MemberDto>()
                .ToListAsync();
        }

        public async Task<IEnumerable<MemberDto>> GetActiveMembers()
        {
            var members = await _dbContext.Members
                .Where(x => x.Active)
                .ProjectTo<MemberDto>()
                .ToListAsync();

            return members.Where(x => !string.IsNullOrEmpty(x.UserEmail)).ToList();
        }

        public async Task<MemberDto> GetMember(int id)
        {
            return await _dbContext.Members
                .Where(x => x.Id == id)
                .ProjectTo<MemberDto>()
                .FirstOrDefaultAsync();
        }

        public async Task UpdateMember(int id, MemberDto member)
        {
            var dbMember = await _dbContext.Members
                .FirstOrDefaultAsync(x => x.Id == id);

            dbMember.FirstName = member.FirstName;
            dbMember.LastName = member.LastName;
            dbMember.Active = member.Active;
            dbMember.Departed = member.Departed;
            dbMember.DepartedReason = member.DepartedReason;

            await _dbContext.SaveChangesAsync();

            if (!string.IsNullOrEmpty(member.UserEmail))
            {
                var user = await _dbContext.Users
                    .FirstOrDefaultAsync(x => x.Id == member.UserId);

                if (user != null)
                {
                    user.Email = member.UserEmail;
                    user.UserName = member.UserEmail;
                    user.NormalizedEmail = member.UserEmail.ToUpper();
                    user.NormalizedUserName = member.UserEmail.ToUpper();

                    await _dbContext.SaveChangesAsync();
                }
                else
                {
                    user = new User
                    {
                        UserName = member.UserEmail,
                        Email = member.UserEmail,
                        EmailConfirmed = true
                    };

                    await _userManager.CreateAsync(user);

                    dbMember = await _dbContext.Members.FirstOrDefaultAsync(x => x.Id == id);

                    dbMember.UserId = user.Id;

                    await _dbContext.SaveChangesAsync();
                }
            }
        }

        public async Task ActivateMember(int id)
        {
            var member = await _dbContext.Members.FirstOrDefaultAsync(x => x.Id == id);

            member.Active = true;

            await _dbContext.SaveChangesAsync();
        }

        public async Task DeactivateMember(int id)
        {
            var member = await _dbContext.Members.FirstOrDefaultAsync(x => x.Id == id);

            member.Active = false;

            await _dbContext.SaveChangesAsync();
        }

        public async Task ResetPassword(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);

            if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
            { 
                _logger.LogDebug($"Did not find user by email {email}");

                throw new Exception("Could not reset password");
            }

            var code = await _userManager.GeneratePasswordResetTokenAsync(user);
            await _emailService.SendForgotPasswordEmail(user.Id, user.Email, code);
        }

        public async Task Invite(string email)
        {
            await CreateUser(email);

            // Now create the member
            var user = await _userManager.FindByEmailAsync(email);

            var member = new Member
            {
                Active = true,
                User = user,
                Joined = DateTime.UtcNow
            };

            _dbContext.Members.Add(member);

            await _dbContext.SaveChangesAsync();
        }

        public async Task AddToAdministrators(int memberId)
        {
            var user = await _dbContext.Users
                .Where(x => x.Member.Id == memberId)
                .FirstOrDefaultAsync();

            if (user == null)
            {
                throw new Exception("User does not exist");
            }

            if (!await _userManager.IsInRoleAsync(user, "Administrator"))
                await _userManager.AddToRoleAsync(user, "Administrator");
        }

        public async Task RemoveFromAdministrators(int memberId)
        {
            var user = await _dbContext.Users
                .Where(x => x.Member.Id == memberId)
                .FirstOrDefaultAsync();

            if (user == null)
            {
                throw new Exception("User does not exist");
            }

            if (await _userManager.IsInRoleAsync(user, "Administrator"))
                await _userManager.RemoveFromRoleAsync(user, "Administrator");
        }

        public async Task<bool> IsAdministrator(int memberId)
        {
            var user = await _dbContext.Users
                .Where(x => x.Member.Id == memberId)
                .FirstOrDefaultAsync();


            return await _userManager.IsInRoleAsync(user, "Administrator");
        }

        private async Task CreateUser(string email)
        {
            var user = new User { UserName = email, Email = email };

            var result = await _userManager.CreateAsync(user);

            if (result.Succeeded)
            {
                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);

                await _emailService.SendWelcomeEmail(user.Id, code, user.Email);

                _logger.LogInformation(3, "User has been invited to join Cake Club.");
            }
            else
            {
                foreach (var error in result.Errors)
                {
                    _logger.LogError(error.Description);
                }

                throw new Exception("Could not invite user");
            }
        }
    }
}
