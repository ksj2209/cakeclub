using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using CakeClub.Entities;
using CakeClub.Models;
using CakeClub.Options;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CakeClub.Infrastructure.Services
{
    public class VoteService
    {
        private const string PURPOSE = "voting";
        private readonly ApplicationDbContext _dbContext;
        private readonly EmailService _emailService;
        private readonly IDataProtectionProvider _provider;
        private readonly IDataProtector _protector;
        private readonly ILogger<VoteService> _logger;
        private readonly VoteOptions _options;
        private readonly UserProvider _userProvider;

        public VoteService(ApplicationDbContext dbContext, EmailService emailService, IDataProtectionProvider provider, IOptions<VoteOptions> options, ILogger<VoteService> logger, UserProvider userProvider)
        {
            _dbContext = dbContext;
            _emailService = emailService;
            _provider = provider;
            _logger = logger;
            _options = options.Value;
            _userProvider = userProvider;

            if (provider == null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            _protector = provider.CreateProtector(PURPOSE);
        }

        public async Task AddVote(int memberId, int cakeId, int? grade, bool participated)
        {
            if (!_userProvider.IsAdministrator()) throw new UnauthorizedAccessException("Only administrators may add votes on behalf of others");

            var member = await _dbContext.Members
                .Where(x => x.Id == memberId)
                .FirstOrDefaultAsync();
            
            var cake = await _dbContext.Cakes
                .Where(x => x.Id == cakeId)
                .FirstOrDefaultAsync();
           
            if (await _dbContext.Votes.AnyAsync(x => x.Cake == cake && x.Member == member)) throw new Exception("A vote for this cake already exist");

            _dbContext.Votes.Add(new Vote
            {
                Member = member,
                Cake = cake,
                Grade = grade,
                Participated = participated
            });

            await _dbContext.SaveChangesAsync();
        }

        public async Task SendVoteEmail(int memberId, int cakeId)
        {
            var member = await _dbContext.Members
                .Where(x => x.Id == memberId)
                .ProjectTo<MemberDto>()
                .FirstOrDefaultAsync();
            
            var cake = await _dbContext.Cakes
                .Where(x => x.Id == cakeId)
                .ProjectTo<CakeDto>()
                .FirstOrDefaultAsync();

            if (member == null) throw new Exception("Member not found");
            if (cake == null) throw new Exception("Cake not found");

            var token = GetVoteToken(member, cake);

            await _emailService.SendVoteEmail(cake, member, token);
        }

        public async Task<bool> HasCurrentMemberVoted(int cakeId)
        {
            var currentMember = await _userProvider.GetCurrentMember();

            return await _dbContext.Votes
                .AnyAsync(x => x.Member.Id == currentMember.Id && x.Cake.Id == cakeId);
        }

        private string GetVoteToken(MemberDto member, CakeDto cake)
        {
            using (var ms = new MemoryStream())
            {
                using (var writer = ms.CreateWriter())
                {
                    writer.Write(DateTimeOffset.UtcNow);
                    writer.Write(member.Id);
                    writer.Write(cake.Id);
                    writer.Write(PURPOSE);
                }

                var protectedBytes = _protector.Protect(ms.ToArray());
                return Convert.ToBase64String(protectedBytes);
            }
        }

        public bool Validate(int memberId, int cakeId, string token)
        {
            try
            {
                var unprotectedData = _protector.Unprotect(Convert.FromBase64String(token));

                using (var ms = new MemoryStream(unprotectedData))
                {
                    using (var reader = ms.CreateReader())
                    {
                        var creationTime = reader.ReadDateTimeOffset();
                        var expirationTime = creationTime + TimeSpan.FromDays(7);

                        _logger.LogDebug(creationTime.ToString());
                        _logger.LogDebug(expirationTime.ToString());

                        if (DateTime.UtcNow > expirationTime)
                        {
                            return false;
                        }

                        var tokenMemberId = reader.ReadInt32();
                        _logger.LogDebug(tokenMemberId.ToString());
                        if (tokenMemberId != memberId)
                        {
                            return false;
                        }

                        var tokenCakeId = reader.ReadInt32();
                        _logger.LogDebug(tokenCakeId.ToString());
                        if (tokenCakeId != cakeId)
                        {
                            return false;
                        }

                        var purpose = reader.ReadString();
                        _logger.LogDebug(purpose.ToString());
                        if (purpose != PURPOSE)
                        {
                            return false;
                        }

                        return reader.PeekChar() == -1;
                    }
                }
            }
            catch (System.Exception)
            {
                // No leaking
            }

            return false;
        }

        /// <summary>
        /// Vote with token (use for anonymous users)
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="cakeId"></param>
        /// <param name="token"></param>
        /// <param name="grade"></param>
        /// <param name="participated"></param>
        /// <returns></returns>
        public async Task Vote(int memberId, int cakeId, string token, int? grade, bool participated)
        {
            if (!this.Validate(memberId, cakeId, token)) throw new Exception("Token is invalid");
            
            var member = await _dbContext.Members
                .FirstOrDefaultAsync(x => x.Id == memberId);

            var cake = await _dbContext.Cakes
                .FirstOrDefaultAsync(x => x.Id == cakeId);

            if (member == null) throw new Exception("Member not found");
            if (cake == null) throw new Exception("Cake not found");
           
            if (await _dbContext.Votes.AnyAsync(x => x.Cake == cake && x.Member == member)) throw new Exception("A vote for this cake already exist");

            var vote = new Vote
            {
                Member = member,
                Cake = cake,
                Grade = grade,
                Participated = participated
            };

            _dbContext.Votes.Add(vote);

            await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Vote without token (only for authenticated users)
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="cakeId"></param>
        /// <param name="grade"></param>
        /// <param name="participated"></param>
        /// <returns></returns>
        public async Task Vote(int memberId, int cakeId, int? grade, bool participated)
        {
            var member = await _dbContext.Members
                .FirstOrDefaultAsync(x => x.Id == memberId);

            var cake = await _dbContext.Cakes
                .FirstOrDefaultAsync(x => x.Id == cakeId);

            if (member == null) throw new Exception("Member not found");
            if (cake == null) throw new Exception("Cake not found");

            if (await _dbContext.Votes.AnyAsync(x => x.Cake == cake && x.Member == member)) throw new Exception("A vote for this cake already exist");

            if (!participated && grade.HasValue)
            {
                throw new Exception("Cannot give a grade without participating");
            }

            var vote = new Vote
            {
                Member = member,
                Cake = cake,
                Grade = grade,
                Participated = participated
            };

            _dbContext.Votes.Add(vote);

            await _dbContext.SaveChangesAsync();
        }
    }

        /// <summary>
    /// Utility extensions to streams
    /// </summary>
    internal static class StreamExtensions
    {
        internal static readonly Encoding DefaultEncoding = new UTF8Encoding(false, true);

        public static BinaryReader CreateReader(this Stream stream)
        {
            return new BinaryReader(stream, DefaultEncoding, true);
        }

        public static BinaryWriter CreateWriter(this Stream stream)
        {
            return new BinaryWriter(stream, DefaultEncoding, true);
        }

        public static DateTimeOffset ReadDateTimeOffset(this BinaryReader reader)
        {
            return new DateTimeOffset(reader.ReadInt64(), TimeSpan.Zero);
        }

        public static void Write(this BinaryWriter writer, DateTimeOffset value)
        {
            writer.Write(value.UtcTicks);
        }
    }
}
