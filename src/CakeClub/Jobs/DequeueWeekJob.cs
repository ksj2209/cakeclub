﻿using CakeClub.Infrastructure.Services;
using FluentScheduler;
using Microsoft.Extensions.Logging;

namespace CakeClub.Jobs
{
    public class DequeueWeekJob : IJob
    {
        private readonly ILogger<DequeueWeekJob> _logger;
        private readonly CakeService _cakeService;

        public DequeueWeekJob(ILogger<DequeueWeekJob> logger, CakeService cakeService)
        {
            _logger = logger;
            _cakeService = cakeService;
        }

        public void Execute()
        {
            _logger.LogInformation($"Started executing {nameof(DequeueWeekJob)}");

            _cakeService.NextWeek().GetAwaiter().GetResult();

            _logger.LogInformation($"Finished executing {nameof(DequeueWeekJob)}");
        }
    }
}
