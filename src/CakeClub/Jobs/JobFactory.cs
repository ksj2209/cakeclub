using System;
using FluentScheduler;
using Microsoft.Extensions.DependencyInjection;

namespace CakeClub.Jobs
{
    public class JobFactory : IJobFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public JobFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IJob GetJobInstance<T>() where T: IJob
        {
            return _serviceProvider.GetRequiredService<T>();
        }
    }
}