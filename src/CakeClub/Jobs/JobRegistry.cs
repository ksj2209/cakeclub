using System;
using FluentScheduler;

namespace CakeClub.Jobs
{
    public class JobRegistry : Registry
    {
        public JobRegistry()
        {
            var date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var previousMonday = date.Subtract(TimeSpan.FromDays((6 + (int)DateTime.Now.DayOfWeek) % 7));
            var nextMonday = previousMonday.Add(TimeSpan.FromDays(7));
            var nextMondayMorning = nextMonday.AddHours(8);

            Schedule<DequeueWeekJob>().ToRunOnceAt(nextMonday).AndEvery(7).Days();
            Schedule<SendRemindersJob>().ToRunOnceAt(nextMondayMorning).AndEvery(7).Days();
            Schedule<SendVotesJob>().ToRunOnceAt(DateTime.UtcNow).AndEvery(15).Minutes();
        }
    }
}