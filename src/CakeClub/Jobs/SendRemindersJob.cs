using System;
using System.Linq;
using System.Threading.Tasks;
using CakeClub.Infrastructure;
using CakeClub.Infrastructure.Services;
using FluentScheduler;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace CakeClub.Jobs
{
    public class SendRemindersJob : IJob
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly CakeService _cakeService;
        private readonly UserService _userService;
        private readonly EmailService _emailService;
        private readonly ILogger<SendRemindersJob> _logger;

        public SendRemindersJob(ApplicationDbContext dbContext, CakeService cakeService, UserService userService, EmailService emailService, ILogger<SendRemindersJob> logger)
        {
            _dbContext = dbContext;
            _cakeService = cakeService;
            _userService = userService;
            _emailService = emailService;
            _logger = logger;
        }
        
        public void Execute()
        {
            _logger.LogInformation($"Started executing {nameof(SendRemindersJob)}");

            ExecuteAsync().GetAwaiter().GetResult();

            _logger.LogInformation($"Finished executing {nameof(SendRemindersJob)}");
        }

        private async Task ExecuteAsync()
        {
            var weeks = await _cakeService.GetWeeks();

            var nextThreeWeeks = weeks.Where(x => x.StartDate < DateTime.UtcNow.AddDays(21) && x.BakerId.HasValue);

            foreach (var week in nextThreeWeeks)
            {
                if (week != null && !week.IsEventCreated)
                {
                    var member = await _userService.GetMember(week.BakerId.Value);

                    await _emailService.SendReminderEmail(member, week.StartDate);

                    await _cakeService.IncrementRemindersSent(week.Id);
                }
            }
        }
    }
}