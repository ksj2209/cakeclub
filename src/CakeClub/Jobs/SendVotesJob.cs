using System;
using System.Linq;
using System.Threading.Tasks;
using CakeClub.Infrastructure;
using CakeClub.Infrastructure.Services;
using FluentScheduler;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace CakeClub.Jobs
{
    public class SendVotesJob : IJob
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly CakeService _cakeService;
        private readonly VoteService _voteService;
        private readonly UserService _userService;
        private readonly EmailService _emailService;
        private readonly ILogger<SendVotesJob> _logger;

        public SendVotesJob(ApplicationDbContext dbContext, CakeService cakeService, VoteService voteService, UserService userService, EmailService emailService, ILogger<SendVotesJob> logger)
        {
            _dbContext = dbContext;
            _cakeService = cakeService;
            _voteService = voteService;
            _userService = userService;
            _emailService = emailService;
            _logger = logger;
        }
        
        public void Execute()
        {
            _logger.LogInformation($"Started executing {nameof(SendVotesJob)}");

            ExecuteAsync().GetAwaiter().GetResult();

            _logger.LogInformation($"Finished executing {nameof(SendVotesJob)}");
        }

        private async Task ExecuteAsync()
        {
            var cakesThatHaveNotBeenVotedOn = await _dbContext.Cakes
                .Where(x => !x.VotesSent)
                .ToListAsync();

            var activeMembers = await _userService.GetActiveMembers();

            foreach (var cake in cakesThatHaveNotBeenVotedOn)
            {
                foreach (var member in activeMembers)
                {
                    await _voteService.SendVoteEmail(member.Id, cake.Id);
                }

                cake.VotesSent = true;
            }

            await _dbContext.SaveChangesAsync();
        }
    }
}