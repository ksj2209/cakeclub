using System.ComponentModel.DataAnnotations;

namespace CakeClub.Models.AccountViewModels
{
    public class SetupViewModel
    {
        [Required]
        // [StringLength(500, ErrorMessage= "The {0} must be max {1) characters long.")]
        public string FirstName { get; set; }
        
        [Required]
        // [StringLength(500, ErrorMessage= "The {0} must be max {1) characters long.")]
        public string LastName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
