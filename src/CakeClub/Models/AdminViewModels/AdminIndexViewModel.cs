using System.Collections.Generic;

namespace CakeClub.Models.AdminViewModels
{
    public class AdminIndexViewModel
    {
        public IEnumerable<MemberDto> Members { get; set; } = new List<MemberDto>();
    }
}