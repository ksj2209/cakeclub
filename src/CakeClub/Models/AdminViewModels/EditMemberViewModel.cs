﻿namespace CakeClub.Models.AdminViewModels
{
    public class EditMemberViewModel
    {
        public MemberDto Member { get; set; }
        public bool IsAdmin { get; set; }
    }
}
