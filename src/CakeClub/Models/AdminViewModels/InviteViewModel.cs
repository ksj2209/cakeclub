using System.ComponentModel.DataAnnotations;

namespace CakeClub.Models.AdminViewModels
{
    public class InviteViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}