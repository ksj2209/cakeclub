using System;

namespace CakeClub.Models
{
    public class CakeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Recipe { get; set; }
        public bool BakeSelf { get; set; }
        public int CreatorId { get; set; }
        public string CreatorFirstName { get; set; }
        public string CreatorLastName { get; set; }
        public bool CreatorActive { get; set; }
        public DateTime EventTime { get; set; }
        public double? Grade { get; set; }
    }
}