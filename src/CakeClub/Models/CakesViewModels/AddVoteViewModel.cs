using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CakeClub.Models.CakesViewModels
{
    public class AddVoteViewModel
    {
        public IEnumerable<MemberDto> Members { get; set; } = new List<MemberDto>();
        [Required]
        public int MemberId { get; set; }
        [Required]
        public int CakeId { get; set; }
        [Required]
        public int? Grade { get; set; } = null;
        [Required]
        public bool Participated { get; set; }
    }
}
