using System.Collections.Generic;

namespace CakeClub.Models.CakesViewModels
{
    public class CakeViewModel
    {
        public CakeDto Cake { get; set; }
        public IEnumerable<VoteDto> Votes { get; set; }
        public bool HasVoted { get; set; }
    }
}