using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CakeClub.Models.CakesViewModels
{
    public class CakesViewModel
    {
        public IEnumerable<CakeDto> Cakes { get; set; }
        [Required]        
        public string Name { get; set; }
        [Required]
        public DateTime When { get; set; }
    }
}