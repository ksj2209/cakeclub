﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CakeClub.Models.CakesViewModels
{
    public class CreateCakeViewModel
    {
        public IEnumerable<MemberDto> Members { get; set; } = new List<MemberDto>();
        public int? MemberId { get; set; } = null;
        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        [Required]
        [DataType(DataType.Time)]
        public DateTime Time { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public bool BakeSelf { get; set; }
        [Required]
        public bool VotesSent { get; set; }
        public string Recipe { get; set; }
    }
}
