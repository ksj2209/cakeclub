using System;

namespace CakeClub.Models.CakesViewModels
{
    public class GetCakeViewModel
    {
        public int Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Name { get; set; }
    }
}