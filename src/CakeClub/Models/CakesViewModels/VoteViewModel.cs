using System.ComponentModel.DataAnnotations;

namespace CakeClub.Models.CakesViewModels
{
    public class VoteViewModel
    {
        public CakeDto Cake { get; set; }
        public MemberDto Member { get; set; }
        public int? Grade { get; set; } = null;
        [Required]
        public int CakeId { get; set; }
        [Required]
        public int MemberId { get; set; }
        [Required]
        public bool Participated { get; set; } = true;
        public string Token { get; set; }
    }
}