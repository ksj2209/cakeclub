using System;

namespace CakeClub.Models
{
    public class EventDto
    {
        public int Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string CakeName { get; set; }
    }
}