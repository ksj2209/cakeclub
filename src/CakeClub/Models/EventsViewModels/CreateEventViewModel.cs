using System;

namespace CakeClub.Models.EventsViewModels
{
    public class CreateEventViewModel
    {
        public string Name { get; set; }
        public DateTime When { get; set; }
    }
}