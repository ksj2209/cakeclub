using System.Collections.Generic;
using CakeClub.Entities;

namespace CakeClub.Models.EventsViewModels
{
    public class EventsViewModel
    {
        public IEnumerable<Cake> Cakes { get; set; } = new List<Cake>();
    }
}