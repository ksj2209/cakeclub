using System;

namespace CakeClub.Models.EventsViewModels
{
    public class GetEventViewModel
    {
        public int Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Name { get; set; }
    }
}