using System;
using System.Collections.Generic;

namespace CakeClub.Models.HomeViewModels
{
    public class UpcomingViewModel
    {
        public IEnumerable<MemberDto> Members { get; set; }
        public int SelectedMemberId { get; set; }
        public IEnumerable<WeekDto> Weeks { get; set; }
    }
}