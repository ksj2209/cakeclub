using System;

namespace CakeClub.Models
{
    public class MemberDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public DateTime Joined { get; set; }
        public DateTime? Departed { get; set; }
        public string DepartedReason { get; set; }
        public bool Active { get; set; }
        public int? UserId { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
    }
}