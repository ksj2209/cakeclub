using System.Collections.Generic;

namespace CakeClub.Models.StatisticsViewModels
{
    public class GraphViewModel
    {
        public Dictionary<string, IEnumerable<CakeDto>> Data { get; set; } = new Dictionary<string, IEnumerable<CakeDto>>();
    }
}