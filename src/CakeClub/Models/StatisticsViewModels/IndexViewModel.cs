using System.Collections.Generic;

namespace CakeClub.Models.StatisticsViewModels
{
    public class IndexViewModel
    {
        public int ActiveMembers { get; set; }
        public int TotalMembers { get; set; }
        public int TotalCakes { get; set; }
        public int TotalWeeks { get; set; }
        public int RankedCakes { get; set; }
        public double? AverageCakeGrade { get; set; }
        public double? TopCakeGrade { get; set; }
        public double? LowestCakeGrade { get; set; }
        public IList<CakeDto> CurrentRankings { get; set; } = new List<CakeDto>();
    }
}