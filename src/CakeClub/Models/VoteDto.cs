namespace CakeClub.Models
{
    public class VoteDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string MemberFirstName { get; set; }
        public string MemberLastName { get; set; }
        public int CakeId { get; set; }
        public int? Grade { get; set; }
        public bool Participated { get; set; }
    }
}