using System;

namespace CakeClub.Models
{
    public class WeekDto
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public int RemindersSent { get; set; }
        public bool Skip { get; set; }
        public int? BakerId { get; set; }
        public string BakerFirstName { get; set; }
        public string BakerLastName { get; set; }
        public string BakerFullName { get; set; }
        public bool IsEventCreated { get; set; }
    }
}