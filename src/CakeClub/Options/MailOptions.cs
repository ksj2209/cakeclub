namespace CakeClub.Options
{
    public class MailOptions
    {
        public string Sender { get; set; }
        public string Smtp { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}