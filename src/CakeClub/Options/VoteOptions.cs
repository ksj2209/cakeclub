using System;

namespace CakeClub.Options
{
    public class VoteOptions
    {   
        public TimeSpan TokenLifespan { get; set; } = TimeSpan.FromDays(5);
    }
}