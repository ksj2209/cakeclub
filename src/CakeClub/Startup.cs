﻿using AutoMapper;
using CakeClub.Emails;
using CakeClub.Entities;
using CakeClub.Infrastructure;
using CakeClub.Infrastructure.Services;
using CakeClub.Jobs;
using CakeClub.Options;
using FluentEmail.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System.IO;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Threading.Tasks;
using System;

namespace CakeClub
{
    public class Startup
    {
        private IHostingEnvironment _env;

        public Startup(IHostingEnvironment env)
        {
            _env = env;

            var builder = new ConfigurationBuilder();

            if (env.IsProduction())
            {
                builder.SetBasePath("/home/cakeclub/config/");
            }
            else
            {
                builder.SetBasePath(env.ContentRootPath);
            }

            builder.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();

            var config = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .MinimumLevel.Debug();

            if (_env.IsDevelopment())
            {
                config.WriteTo.LiterateConsole();
            }
            else
            {
                config.WriteTo.RollingFile("../../logs/log-{Date}.txt");
            }

            Log.Logger = config.CreateLogger();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging();
            services.AddOptions();
            services.Configure<VoteOptions>(Configuration.GetSection("Voting"));
            services.Configure<MailOptions>(Configuration.GetSection("Mailing"));
            services.Configure<Settings>(Configuration);

            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlite(Configuration.GetConnectionString("CakeClub")));
            
            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(options => 
            {
                options.LoginPath = "/Account/Login";
                options.LogoutPath = "/Account/Logoff";
                options.Events = new CookieAuthenticationEvents
                {
                    OnRedirectToLogin = context => 
                    {
                        // Force https for login redirects
                        context.RedirectUri = context.RedirectUri.Replace("http:", "https:");
                        context.Response.Redirect(context.RedirectUri);
                        return Task.FromResult(0);
                    }
                };
            });

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();

            // Add application services.
            services.AddTransient<EmailService>();
            services.AddTransient<VoteService>();
            services.AddTransient<CakeService>();
            services.AddTransient<UserService>();
            services.AddTransient<UserProvider>();

            // Jobs
            services.AddTransient<DequeueWeekJob>();
            services.AddTransient<SendRemindersJob>();
            services.AddTransient<SendVotesJob>();

            services.AddAutoMapper();

            services.AddMvc();

            Email.DefaultSender = new SmtpSender(Configuration["Mailing:UserName"], Configuration["Mailing:Password"], Configuration["Mailing:Host"], int.Parse(Configuration["Mailing:Port"]));
            Email.DefaultRenderer = new FluentEmail.Core.Defaults.ReplaceRenderer();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.ImportData(env);

            FluentScheduler.JobManager.JobFactory = new JobFactory(app.ApplicationServices);
            FluentScheduler.JobManager.Initialize(new JobRegistry());

            FluentScheduler.JobManager.JobException += (info) => Log.Fatal("An error just ocurred in a scheduled job: " + info.Exception);

            app.UseAuthentication();

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
        }
    }
}
