﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Threading.Tasks;

namespace CakeClub.TagHelpers
{
    [HtmlTargetElement("markdown", TagStructure = TagStructure.NormalOrSelfClosing)]
    [HtmlTargetElement(Attributes = "markdown")]
    public class MarkdownTagHelper : TagHelper
    {
        public async override Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            if (output.TagName == "markdown")
            {
                output.TagName = null;
            }

            output.Attributes.RemoveAll("markdown");
            
            var childContent = await output.GetChildContentAsync(true, NullHtmlEncoder.Default);
            var markdown = System.Net.WebUtility.HtmlDecode(childContent.GetContent(NullHtmlEncoder.Default));
            var html = Markdig.Markdown.ToHtml(markdown);
            output.Content.SetHtmlContent(html ?? "");
        }
    }
}
