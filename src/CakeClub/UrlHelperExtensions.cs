using Microsoft.AspNetCore.Mvc;

namespace CakeClub
{
    static class UrlHelperExtensions
    {
        /// <summary>
        /// Generates a fully qualified URL to an action method by using
        /// the specified action name, controller name and route values.
        /// </summary>
        /// <param name="url">The URL helper.</param>
        /// <param name="actionName">The name of the action method.</param>
        /// <param name="controllerName">The name of the controller.</param>
        /// <param name="routeValues">The route values.</param>
        /// <returns>The absolute URL.</returns>
        public static string AbsoluteAction(this IUrlHelper url, string actionName, string controllerName, object routeValues = null)
        {
            // Force https
            string scheme = "https"; //url.ActionContext.HttpContext.Request.Scheme;
            
            if (url.ActionContext.HttpContext.Request.Headers.TryGetValue("X-Forwarded-Host", out var host))
            {
                return url.Action(actionName, controllerName, routeValues, scheme, host);
            }

            return url.Action(actionName, controllerName, routeValues, scheme);
        }
    }
}